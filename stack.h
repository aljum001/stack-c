/*Mohamed Aljumaily A5 program 1 stack.h file
This file will have the class decleration
for the class stack which will be a 
dynamic stack that will  hold function
to add and remove from the stack.
*/
#ifndef STACK_H
#define STACK_H
using namespace std;
//type def of type char called el_t
typedef char el_t;
class Stack
{
 private:
  //structt that will hold each node
  struct Node
  {
    //variable called element of type char
    el_t element;
    Node* next; //pointer to the next
  };
  //pointer to the top of the stack
  Node* top;
 public:
  Stack(); //constructor
  ~Stack(); //destructor
  void destroy(); //destroy func
  void add(el_t); //adding to stack
  void getTop(el_t&) const; //get value of top
  void remove(); //remove from stack
  bool isEmpty() const; //empty stack func
};

#endif
