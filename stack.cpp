/*Mohamed Aljumaily A5 Program 1 stack.cpp file
this file will hold all the definitions for the 
dynamic Stack which includes add, remove 
and destroy functions
*/
#include "stack.h"
#include <iostream>
using namespace std;

Stack :: Stack()
{ //default constructor. sets pointer top to null
  top = nullptr;
}

Stack :: ~Stack()
{//destructor calls destroy function
  destroy();
}

void Stack :: destroy()
{//makes a pointer and assigns it to top
  Node *nodePtr = top;
  while(nodePtr)
    { //while nodeptr is not null move top
      top = top->next;
      delete nodePtr; //delete nodeptr
      nodePtr = top; //set nodeptr to top to traverse
    }
}

void Stack :: add(char c)
{ //add function adds an element to the stack
  //makes a new pointer to a new node
  Node* newNode = new Node;
  //adds value sent from main to the new node
  newNode -> element = c;
  newNode ->next = nullptr;
  if(isEmpty())
    { //if the stack is empty set top to new node
      top = newNode;
    }
  else
    { //else put the new node at the top of the stack
      newNode ->next = top;
      top = newNode;
    }
}

void Stack :: remove()
{//remove functions removes the top of the stack
  if(isEmpty())
    { //if it's empty printt below
      cout << "Stack is empty. Nothing to remove" << endl;
    }
  else
    { // else make a pointer to top and make top equal the
      //next node and delete the top node
      Node* nodePtr = top;
      top = top -> next;
      delete nodePtr;
    }
}

void Stack :: getTop(char& c) const
{ //gettop function gets the top char in the stack
  if(isEmpty())
    { //if the stack is empty assign c to 0
      c = 0;
    }
  else
    { //else assign it tothe top element
      c = top->element;
    }
}


bool Stack :: isEmpty() const
{ //isempty checks it the stack is empty
  if(!top)
    { //if top is null return true
      return true;
    }
  else 
    { //if it's not return false.
      return false;
    }

}
