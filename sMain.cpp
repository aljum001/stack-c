/*Mohamed Aljumaily A5 Program 1 main file
This program will use a dynamic stack to
find if a statement has proper delimiters or
if it is not balanced. It has a function
that will find it using a string taken 
from the main function and return true or false
*/
#include "stack.h"
#include <iostream>
#include <string>
using namespace std;
//func decleration
bool ProperlyBalanced(string& statement);
int main()
{ //string statement that will hold the string from user
  string statement;
  do //do while loop
    {
      cout << "Enter a statement to check. Enter -1 to stop:";
      cin >> statement;
      //if the function call returns true print below
      if (ProperlyBalanced(statement))
	{
	  cout << "The statement is balanced." << endl;
	}
      else //if it's false print below
	{
	  cout << "The statement is not balanced." << endl;
	} //while the user does not input -1
    }while(statement != "-1");
}

//func definition
bool ProperlyBalanced(string& statement)
{ //declaring stack S
  Stack S;
  char c; //character C
  for(int i = 0; i < statement.size(); i++)
    { //for loop for rthe size of the string
      //if it is an opening delemiter
      if((statement[i] == '(') || (statement[i] == '[') ||
	 (statement[i] == '{'))
	{ //add it to the Stack
	  S.add(statement[i]);
	} //if it is a closing delemiter
      else if ((statement[i] == ')') || (statement[i] == ']') ||
	       (statement[i] == '}'))
	{
	  S.getTop(c); //set the top value of stack to c
	  if(c == 0) // if the stack is empty 
	    {//print below and return false to main
	      cout << "Delimiters not balanced. Extra closing delimiters."
		   << endl;
	      return false;
	    }
	  else
	    { //if it is an opening parantheses and they match
	      if(c == '(' && statement[i] == ')')
		{ //remove the top element of the stack
		  S.remove();
		}
	      else if (c == '[' && statement[i] == ']')
		{ //if its an opening bracket and they match
		  S.remove(); //remove top element of stack
		}
	      else if (c == '{' && statement[i] == '}')
		{ //if it's an opening bracket and they match
		  S.remove(); //remove top element ofthe stack
		}
	      else
		{ //else they don't match, print below and return false
		  cout << "Delimiters do not match." << endl;
		  return false;
		}
	    }
	}
    } //check once for loop is done if the stack is empty
  if(S.isEmpty())
    { //if true the statement is balanced return true
      return true;
    }
  else
    { //else print below and return false.
      cout << "Delimiters do not match. Extra opening delimiter."
	   << endl;
      return false;
    }
}
